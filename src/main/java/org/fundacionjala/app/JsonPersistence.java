package org.fundacionjala.app;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;

import com.google.gson.Gson;
import com.google.gson.JsonIOException;
import com.google.gson.stream.JsonReader;
import org.fundacionjala.app.quizz.model.Quiz;
import org.fundacionjala.app.quizz.model.QuizAnswers;

public class JsonPersistence {

    public static QuizAnswers readJsonFile() {
        Gson gson = new Gson();
        QuizAnswers quiz = null;
        try (JsonReader reader = new JsonReader(new FileReader("./myForm.json"))) {
            quiz = gson.fromJson(reader, QuizAnswers.class);
        } catch (IOException exception) {
            exception.printStackTrace();
        }

        return quiz;
    }

    public static void writeJsonFile(QuizAnswers quizz) {
        Gson gson = new Gson();
        try (Writer writer = new FileWriter("./myForm.json")) {
            gson.toJson(quizz, writer);
        } catch (JsonIOException | IOException exception) {
            exception.printStackTrace();
        }
    }


}
