package org.fundacionjala.app.quizz.model.validator;

import java.util.List;

public class UppercaseValidator implements Validator{
    public static final String ERROR_MESSAGE = "input must only contain uppercase letters ";

    @Override
    public void validate(String value, String conditionValue, List<String> errors) {
        if (!value.equals(value.toUpperCase())){
            errors.add(ERROR_MESSAGE + conditionValue);
        }
    }
}
